﻿using System;
using System.Security.Cryptography;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Text;
using Android.Util;
using Android.Widget;
using Xamarin.Essentials;

namespace PasswordGenerator
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private readonly RandomNumberGenerator _randomNumberGenerator = RandomNumberGenerator.Create();

        private EditText _editTextPasswordLength, _editTextGeneratedPassword;
        private Button _buttonGenerate;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Platform.Init(this, savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            FindViewById<Button>(Resource.Id.buttonGenerate).Click += OnButtonGenerateClicked;
            _editTextPasswordLength = FindViewById<EditText>(Resource.Id.editTextPasswordLength);
            _editTextPasswordLength.TextChanged += OnTextPasswordLengthTextChanged;

            _editTextGeneratedPassword = FindViewById<EditText>(Resource.Id.editTextGeneratedPassword);
            _editTextGeneratedPassword.Click += EditTextGeneratedPasswordOnClick;

            _buttonGenerate = FindViewById<Button>(Resource.Id.buttonGenerate);
        }

        private void EditTextGeneratedPasswordOnClick(object sender, EventArgs e)
        {
            ClipboardText = _editTextGeneratedPassword.Text;
        }

        private void OnTextPasswordLengthTextChanged(object sender, TextChangedEventArgs e)
        {
            _editTextGeneratedPassword.Text = "";
            _buttonGenerate.Enabled =
                _editTextPasswordLength.Text.Length > 0 && int.Parse(_editTextPasswordLength.Text) > 0;
        }

        private void OnButtonGenerateClicked(object sender, EventArgs args)
        {
            var passwordLength = int.Parse(_editTextPasswordLength.Text);
            var password = GeneratePassword(passwordLength);

            _editTextGeneratedPassword.Text = password;
            ClipboardText = password;
        }

        private string GeneratePassword(int passwordLength)
        {
            var length = (int) Math.Ceiling(0.75 * passwordLength);
            var byteArray = new byte[length];
            _randomNumberGenerator.GetBytes(byteArray);

            return Base64.EncodeToString(byteArray, Base64Flags.Default).Substring(0, passwordLength);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private string ClipboardText
        {
            get => Clipboard.GetTextAsync().Result;
            set
            {
                Clipboard.SetTextAsync(value).Wait();
                Toast.MakeText(this, "Password copied to clipboard", ToastLength.Long).Show();
            }
        }
    }
}